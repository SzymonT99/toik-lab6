package com.java;

class Main {

    /**
     * metoda main powinna implementowac algorytm do
     * jak najszybszego wyszukiwania wartosci
     * zmiennej digit z klasy QuizImpl (zakladamy ze
     * programista nie zna zawartosci klasy QuizImpl).
     * Nalezy zalozyc, ze pole digit w klasie QuizImpl
     * moze w kazdej chwili ulec zmianie. Do wyszukiwania
     * odpowiedniej wartosci nalezy wykorzystywac tylko
     * i wylacznie metode isCorrectValue - jesli metoda
     * przestanie rzucac wyjatki wowczas mamy pewnosc ze
     * poszukiwana zmienna zostala odnaleziona.
     */
    public static void main(String[] args) {

        Quiz quiz = new QuizImpl();

        int miniRange = Quiz.MIN_VALUE;
        int maxRange = Quiz.MAX_VALUE;
        int digit = (miniRange + maxRange) / 2;


        for (int counter = 1; ; counter++) {

            try {
                quiz.isCorrectValue(digit);
                System.out.println("Trafiona proba!!! Szukana liczba to: "
                        + digit + " Ilosc prob: " + counter);
                break;

            } catch (Quiz.ParamTooLarge l) {
                System.out.println("Argument za duzy!!!");
                maxRange = digit;
                digit = (miniRange + maxRange) / 2;

            } catch (Quiz.ParamTooSmall s) {
                System.out.println("Argument za maly!!!");
                miniRange = digit;
                digit = (miniRange + maxRange) / 2;
            }
        }
    }
}